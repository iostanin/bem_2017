#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <cstring>
#include <fstream>

using std::ifstream;
using std::cout;
using std::endl;

void load_main_config(); // Load main configuration file


#endif // CONFIG_H
