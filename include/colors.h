#ifndef COLORS_H
#define COLORS_H

#include <string>

//----Terminal color manipulators-----
const std::string red("\033[1;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string purple("\033[1;35m");
const std::string cyan("\033[1;36m");
const std::string reset("\033[0m");
//====================================

#endif // COLORS_H
