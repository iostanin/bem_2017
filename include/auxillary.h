#ifndef AUXILLARY_H
#define AUXILLARY_H

#include <iostream>
#include <vector>
#include <cmath>

#include <omp.h>
#include <mpi.h>

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;


// 7) Useful memory structures for triangle mesh
typedef struct triangle_s {

        int id;

        double v1[3];
        double v2[3];
        double v3[3];

        ivec neighbors;

} triangle;

typedef std::vector<triangle> triangle_mesh;


typedef struct bound_conds_s {

        int bc_type;
        double bc_value[3];

} bound_conds;

typedef std::vector<bound_conds> bc_vec;



// Set of useful functions utilized throughout the project


// 1) vector reshape functions

int ii(int m,int n,int M,int N); // 2D
int iii(int m,int n,int k,int M,int N,int K); // 3D
int iiii(int m,int n,int k,int l,int M,int N,int K,int L); // 4D
int iiiii(int m,int n,int k,int l,int p,int M,int N,int K,int L,int P); // 5D

ivec mnk(int i, int M,int N,int K); //3D


// 2) Normal components by three points of triangle
dvec norm(dvec & v1, dvec & v2, dvec & v3);

// 3) Area of triangle by its three components
double area(dvec & v1, dvec & v2, dvec & v3);

// 4) Print out a vector values(structures)
void print_dvec( char * name, dvec & vec);
void print_dvec3( char * name, dvec & vec);
void print_ivec( char * name, ivec & vec);
void print_bc_vec( char * name, bc_vec & conds);
void print_triangle_mesh( char * name, triangle_mesh & mesh);




// 5) MPI scatter of a vector from a main process to all processes
void mpi_scatter_dvec3(MPI_Comm, dvec & );
void mpi_scatter_ivec(MPI_Comm, ivec & );

// 6) MPI gather of a vector from all processes to a main process
void mpi_gather_dvec(MPI_Comm, dvec &, int);
void mpi_gather_ivec(MPI_Comm, dvec &, int);




#endif // AUXILLARY_H
