#ifndef ELAST_TESTS_H
#define ELAST_TESTS_H

#include <iostream>
#include <vector>
#include <omp.h>
#include <mpi.h>
#include <math.h>


#include "colors.h"
#include "bvp.h"
#include "auxillary.h"

#include "solver_ksp.h"
#include "stl_io.h"



void run_sph_test_elast(MPI_Comm);
void run_cub_test_elast(MPI_Comm);

#endif // ELAST_TESTS_H
