// This set of functions defines the uniform tension problem
// that is used to test our FMM codes

#ifndef BVP_H
#define BVP_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include "auxillary.h"

using std::cout;
using std::endl;
using std::sqrt;

typedef  std::vector<double> dvec;
typedef  std::vector<int>    ivec;


void generate_vertices(
                        int N,
                        triangle_mesh & mesh
                      );

// This function sets up a bvp to solve with
// surface solve module

void set_neighbors(triangle_mesh & mesh, double R);

void set_cub_bvp(

        int N,

        triangle_mesh &mesh,  // Mesh

        bc_vec &bcs  // Bondary conditions

        );
/*

void set_sph_bvp( // pure Dirichlet sphere problem

        int N,

        // General BVP parameters
        dvec & vert_1,
        dvec & vert_2,
        dvec & vert_3,
        ivec & bc_type,
        dvec & tr_di);
*/

#endif // BVP_H
