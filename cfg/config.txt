MAIN CONFIGURATION FILE

Sets few important parameters

I. Working mode:

mode = 1

    The following modes are available:
        1) mode = 1: Run a set of validation test problems
           and measure the performance

        2) mode = 2: Solve a boundary value problem

        3) mode = 3: Optimize shape

        4) mode = 4: Optimize topology

        5) mode = 5: Mixed optimization

II. Kernel type

kernel = 1

    The following kernels and BVPs are available:

        1) kernel = 1: Elasticity

        2) kernel = 2: Electrostatic

        3) kernel = 3: Stokes flow

III. Number of OMP threads

omp = 2

IV. PVFMM multipole order

(Even number, for decent precision should not be less than 6 )

mult_order = 6

V. FMM summation / Direct summation in matrix-vector products

summation = 2

    1 - direct summation,
    2 - kifmm summation

VI. Spatial definition

N = 2



    The (0,1)^3 domain studied is divided on N X N X N cells

