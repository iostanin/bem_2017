#include "singular_integrals.h"


///////////////////////////////////////////////////////////////////////////////

// 7) Analytical integration for singular integral with p
double sint_p(int i, int j, const dvec & x1, const dvec & x2,
              const dvec & x3, const dvec & csi, double nu)
{
    double I = 0;
    dvec x_1(3);
    dvec x_2(3);
    dvec x_3(3);
    dvec r1(3);
    dvec r2(3);
    dvec r12(3);
    double norm_r12, norm_r1, norm_r2;
    double dot_r12_r1, dot_r12_r2, ee;
    double cosa1, cosa2, csi_1, csi_2;
    if (i!=j){
        for (int k = 0; k<3; k++)
        {
            if (k==0){ x_1 = x1; x_2 = x2; x_3 = x3;}
            if (k==1){ x_1 = x3; x_2 = x1; x_3 = x2;}
            if (k==2){ x_1 = x2; x_2 = x3; x_3 = x1;}
            for (int v = 0; v<3; v++){
            r1[v] = x_1[v] - csi[v]; r2[v] = x_2[v] - csi[v]; r12[v] = x_1[v] - x_2[v];}
            norm_r12 = std::sqrt(r12[0]*r12[0] + r12[1]*r12[1] + r12[2]*r12[2]);
            norm_r1  = std::sqrt(r1[0]*r1[0] + r1[1]*r1[1] + r1[2]*r1[2]);
            norm_r2  = std::sqrt(r2[0]*r2[0] + r2[1]*r2[1] + r2[2]*r2[2]);
            dot_r12_r1 = r12[0]*r1[0] + r12[1]*r1[1] + r12[2]*r1[2];
            dot_r12_r2 = r12[0]*r2[0] + r12[1]*r2[1] + r12[2]*r2[2];
            if ((i==1) && (j==2)){ ee = r12[2] / norm_r12;}
            if ((i==1) && (j==3)){ ee = - r12[1] / norm_r12;}
            if ((i==2) && (j==3)){ ee = r12[0] / norm_r12;}
            if ((i==2) && (j==1)){ ee = - r12[2] / norm_r12;}
            if ((i==3) && (j==1)){ ee =   r12[1] / norm_r12;}
            if ((i==3) && (j==2)){ ee = - r12[0] / norm_r12;}
            cosa1 =   dot_r12_r1 / (norm_r12*norm_r1);
            cosa2 = - dot_r12_r2 / (norm_r12*norm_r2);
            csi_1 =   norm_r1 * cosa1;
            csi_2 = - norm_r2 * cosa2;
            I = I + ee * (std::log(csi_2 + norm_r2) - std::log(csi_1 + norm_r1));
        }
        I *= (1.0 - 2.0*nu) / ( 8.0 * M_PI * (1.0 - nu) );
    }
    return I;
}



// 8b) Analytical integration for singular integral with u - extended for points beyond the triangle
double sint_u(int i, int j, const dvec & x1, const dvec & x2,
              const dvec & x3, const dvec & csi, double G, double nu)
{
    double I = 0;
    dvec x_1(3);
    dvec x_2(3);
    dvec x_3(3);
    dvec r1(3);
    dvec r2(3);
    dvec r12(3);
    dvec r21(3);
    dvec r31(3);
    dvec e1k(3);
    dvec e2k(3);
    dvec cross_r21_r31(3);
    double a1, a2, a3, b1, b2, b3;
    double norm_r12, norm_r1, norm_r2, dot_r12_r1, dot_r12_r2;
    double theta1, theta2, D, norm_cross_r21_r31;
    for (int k = 0; k<3; k++){
        if (k==0){ x_1 = x1; x_2 = x2; x_3 = x3;}
        if (k==1){ x_1 = x3; x_2 = x1; x_3 = x2;}
        if (k==2){ x_1 = x2; x_2 = x3; x_3 = x1;}
        for (int v = 0; v<3; v++){
            r1[v] = x_1[v] - csi[v];
            r2[v] = x_2[v] - csi[v];
            r12[v] = x_1[v] - x_2[v];
            r21[v] = x_2[v] - x_1[v];
            r31[v] = x_3[v] - x_1[v];}
        norm_r12 = std::sqrt(r12[0]*r12[0] + r12[1]*r12[1] + r12[2]*r12[2]);
        norm_r1 = std::sqrt(r1[0]*r1[0] + r1[1]*r1[1] + r1[2]*r1[2]);
        norm_r2 = std::sqrt(r2[0]*r2[0] + r2[1]*r2[1] + r2[2]*r2[2]);
        dot_r12_r1 = r12[0]*r1[0] + r12[1]*r1[1] + r12[2]*r1[2];
        dot_r12_r2 = r12[0]*r2[0] + r12[1]*r2[1] + r12[2]*r2[2];
        a1 = r21[0]; a2 = r21[1]; a3 = r21[2];
        b1 = r31[0]; b2 = r31[1]; b3 = r31[2];
        cross_r21_r31[0] = a2*b3-a3*b2;
        cross_r21_r31[1] = a3*b1-a1*b3;
        cross_r21_r31[2] = a1*b2-a2*b1;
        norm_cross_r21_r31 = std::sqrt(cross_r21_r31[0]*cross_r21_r31[0] + cross_r21_r31[1]*cross_r21_r31[1] + cross_r21_r31[2]*cross_r21_r31[2]);
        for (int v = 0; v<3; v++)
        { cross_r21_r31[v] = cross_r21_r31[v] / norm_cross_r21_r31;}
        for (int v = 0; v<3; v++) { e1k[v] = r12[v] / norm_r12;}
        a1 = cross_r21_r31[0]; a2 = cross_r21_r31[1]; a3 = cross_r21_r31[2];
        b1 = e1k[0]; b2 = e1k[1]; b3 = e1k[2];
        e2k[0] = a2*b3-a3*b2; e2k[1] = a3*b1-a1*b3; e2k[2] = a1*b2-a2*b1;
        theta1 = std::atan2( e1k[0]*r1[0] + e1k[1]*r1[1] + e1k[2]*r1[2], e2k[0]*r1[0] + e2k[1]*r1[1] + e2k[2]*r1[2]);
        theta2 = std::atan2( e1k[0]*r2[0] + e1k[1]*r2[1] + e1k[2]*r2[2], e2k[0]*r2[0] + e2k[1]*r2[1] + e2k[2]*r2[2]);
        D = e2k[0]*r1[0] + e2k[1]*r1[1] + e2k[2]*r1[2];
        I = I + D * (  (3.0-4.0*nu)/(4*(1.0-nu)) * deltaij(i,j) * std::log( std::fabs(std::tan(theta2) + (1.0/std::cos(theta2))) ) +
        (1.0/(4.0*(1.0-nu))) * ( e1k[i-1]*e1k[j-1]*( - std::sin(theta2) + std::log(std::fabs( std::tan(theta2) + (1.0/std::cos(theta2)) ))) +
        e2k[i-1]*e2k[j-1] * std::sin(theta2) - (e1k[i-1]*e2k[j-1] + e2k[i-1]*e1k[j-1]) * std::cos(theta2) )     );
        I = I - D * (  (3.0-4.0*nu)/(4*(1.0-nu)) * deltaij(i,j) * std::log(std::fabs( std::tan(theta1) + (1.0/std::cos(theta1))) ) +
        (1.0/(4.0*(1.0-nu))) * ( e1k[i-1]*e1k[j-1]*( - std::sin(theta1) + std::log(std::fabs( std::tan(theta1) + (1.0/std::cos(theta1))) )) +
        e2k[i-1]*e2k[j-1] * std::sin(theta1) - (e1k[i-1]*e2k[j-1] + e2k[i-1]*e1k[j-1]) * std::cos(theta1) )     );
    }
    I *= ( - 1.0 / (4.0 * M_PI * G));
    return I;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file.
