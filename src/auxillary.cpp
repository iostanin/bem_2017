#include "auxillary.h"
#include "colors.h"

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;
using std::endl;
using std::cout;

int max_rws = 40;

// Column major vectorization

// 2D array
int ii(int m,int n,int M,int N)
{
    if ((m<0)||(n<0)) return -1;
    if ((m>M-1)||(n>N-1)) return -2;
    return n + N * m;
}

// 3D array
int iii(int m,int n,int k,int M,int N,int K)
{
    if ((m<0)||(n<0)||(k<0)) return -1;
    if ((m>M-1)||(n>N-1)||(k>K-1)) return -2;
    return k + K * n + K * N * m;
}

ivec mnk(int i, int M,int N,int K)
{
    ivec mnk(3);
    mnk[0] = int ( std::floor( i / (K * N) ) );
    mnk[1] = int ( std::floor( (i - mnk[0] * K * N ) / K ) );
    mnk[2] = i - mnk[0] * K * N - mnk[1] * K;
    return mnk;
}

// 4D array
int iiii(int m,int n,int k,int l,int M,int N,int K,int L)
{
    if ((m<0)||(n<0)||(k<0)||(l<0)) return -1;
    if ((m>M-1)||(n>N-1)||(k>K-1)||(l>L-1)) return -2;
    return l + L * k + L * K * n + L * K * N * m;
}

// 5D array
int iiiii(int m,int n,int k,int l,int p,int M,int N,int K,int L,int P)
{
    if ((m<0  )||(n<0  )||(k<0  )||(l<0  )||(p<0  )) {return -1; std::cout<<"error -1"<<std::endl;}
    if ((m>M-1)||(n>N-1)||(k>K-1)||(l>L-1)||(p>P-1)) {return -2; std::cout<<"error -2"<<std::endl;}
    return p + P * l + P * L * k + P * L * K * n + P * L * K * N * m;
}

// Normal components by three points of triangle
dvec norm(dvec & v1, dvec & v2, dvec & v3)
{
    double a1, a2, a3, b1, b2, b3;
    dvec norm(3);

    a1 = v2[0] - v1[0];
    a2 = v2[1] - v1[1];
    a3 = v2[2] - v1[2];

    b1 = v3[0] - v1[0];
    b2 = v3[1] - v1[1];
    b3 = v3[2] - v1[2];

    norm[0] =  a2*b3-a3*b2;
    norm[1] =  a3*b1-a1*b3;
    norm[2] =  a1*b2-a2*b1;

    double len = std::sqrt( norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2] );
    norm[0] = norm[0] / len;
    norm[1] = norm[1] / len;
    norm[2] = norm[2] / len;
    return norm;
}

double area(dvec & v1, dvec & v2, dvec & v3)
{
    double a1, a2, a3, b1, b2, b3, c1, c2, c3;
    double area(3);

    a1 = v2[0] - v1[0];
    a2 = v2[1] - v1[1];
    a3 = v2[2] - v1[2];

    b1 = v3[0] - v1[0];
    b2 = v3[1] - v1[1];
    b3 = v3[2] - v1[2];

    c1 =  a2*b3-a3*b2;
    c2 =  a3*b1-a1*b3;
    c3 =  a1*b2-a2*b1;

    area = std::sqrt(c1*c1 + c2*c2 + c3*c3) / 2.;

    return area;
}

void print_dvec3( char * name, dvec & vec)
{
    cout.precision(3);
    int rows = vec.size()/3; bool over = false;
    if (rows > max_rws) { rows = max_rws; over = true; }

    cout << cyan << name << ":" << reset <<endl;

    for (int i = 0; i<rows; i++){
        cout << yellow << name <<"["<<i<<"]" << reset << " = ("
             << vec[3*i+0] <<" , "<< vec[3*i+1] <<" , "<< vec[3*i+2] << ")"<<endl;
    }   if (over) cout << yellow <<"..."<<reset<<endl<<endl; cout << endl;

}


void print_bc_vec( char * name, bc_vec & conds)
{
    cout.precision(3);
    int rows = conds.size(); bool over = false;
    if (rows > max_rws) { rows = max_rws; over = true; }

    cout << cyan <<"Boundary conditions for "<< yellow << name << ":" << reset <<endl;

    for (int i = 0; i<rows; i++){

        cout << yellow << name << green << ".bc_type = " << reset
             << conds[i].bc_type <<
             ", "<< yellow << name << green << ".bc_value = " << reset <<"(" <<
             conds[i].bc_value[0] <<", " <<
             conds[i].bc_value[1] <<", " <<
             conds[i].bc_value[2] <<")" << reset <<endl;
    }   if (over) cout << yellow <<"..."<<reset<<endl<<endl; cout << endl;

}


void print_triangle_mesh( char * name, triangle_mesh & mesh)
{
    cout.precision(3);
    int rows = mesh.size(); bool over = false;
    if (rows > max_rws) { rows = max_rws; over = true; }
    cout <<endl;
    cout << cyan <<"Triangle mesh for "<< yellow << name << ":" << reset <<endl;

    for (int i = 0; i<rows; i++){

       cout << yellow << name << green << ".id = " << reset
             << mesh[i].id << reset <<endl;


       cout << yellow << name << green <<".v1 = " << reset << "(" <<
                     mesh[i].v1[0] <<", " <<
                     mesh[i].v1[1] <<", " <<
                     mesh[i].v1[2] <<")" << reset <<endl;

       cout << yellow << name << green <<".v2 = " << reset << "(" <<
                     mesh[i].v2[0] <<", " <<
                     mesh[i].v2[1] <<", " <<
                     mesh[i].v2[2] <<")" << reset <<endl;

       cout << yellow << name << green <<".v3 = " << reset << "(" <<
                     mesh[i].v3[0] <<", " <<
                     mesh[i].v3[1] <<", " <<
                     mesh[i].v3[2] <<")" << reset <<endl;

       cout << yellow << name << green <<".neighbors.size() = " << reset << mesh[i].neighbors.size() <<reset<<endl;
       cout << yellow << name << green <<".neighbors = " << reset <<"{";
                   for (int j = 0; j<mesh[i].neighbors.size(); j++)
                    cout << mesh[i].neighbors[j]<<", ";
       cout <<"}" << reset << endl;


    }   if (over) cout << yellow <<"..."<<reset<<endl<<endl; cout << endl;

}




void print_dvec( char * name, dvec & vec)
{
    cout.precision(3);
    int rows = vec.size(); bool over = false;
    if (rows > max_rws) { rows = max_rws; over = true; }

    cout << cyan << name << ":" << reset <<endl;

    for (int i = 0; i<rows; i++){
        cout << yellow << name <<"["<<i<<"]" << reset << "=" <<
             vec[i] <<endl;
    }   if (over) cout << yellow << "..." <<reset<<endl<<endl; cout << endl;

}

void print_ivec( char * name, ivec & vec)
{
    cout.precision(3);
    int rows = vec.size(); bool over = false;
    if (rows > max_rws) { rows = max_rws; over = true; }

    cout << cyan << name << ":" << reset <<endl;

    for (int i = 0; i<rows; i++){
        cout << yellow << name <<"["<<i<<"]" << reset << "=" <<
             vec[i] <<endl;
    }   if (over) cout << yellow <<"..."<<reset<<endl<<endl; cout << endl;

}

void mpi_scatter_dvec3(MPI_Comm comm, dvec & vec)
{
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread

    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    int N_trg = vec.size()/3;
    int start, end, span, step;
    if (n_proc == 1) step = N_trg;
    if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
    start = step * i_proc;
    end   = step *(i_proc+1);
    if (i_proc == n_proc-1) end = N_trg;
    span = end - start;
    for (int i = 0; i<span; i++)
    {
        for (int k=0;k<3;k++) vec[3*i+k] = vec[3*start+3*i+k];
    }
    vec.resize(3*span);
}



void mpi_scatter_ivec(MPI_Comm comm, ivec & vec)
{
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread

    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    int N_trg = vec.size();
    int start, end, span, step;
    if (n_proc == 1) step = N_trg;
    if (n_proc > 1) step = (int) std::floor(N_trg / (double) (n_proc));
    start = step * i_proc;
    end   = step *(i_proc+1);
    if (i_proc == n_proc-1) end = N_trg;
    span = end - start;

    for (int i = 0; i<span; i++)
    {
        vec[i] = vec[start+i];
    }
    vec.resize(span);

}


void mpi_gather_dvec(MPI_Comm comm, dvec & vec, int N)
{
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread

    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    int sendcount = vec.size();
    double sendarray[sendcount];
    for (int i = 0; i<sendcount; i++)
    {
        sendarray[i] = vec[i];
    }
    double rbuf[N];
    int recvcounts[n_proc];
    int displs[n_proc];
    for (int i = 0; i<n_proc; i++)
    {
        int start, end, span, step;
        if (n_proc==1) step = N;
        if (n_proc>1) step = (int) std::floor(N / (double) (n_proc));
        start = step * i;
        end   = step *(i+1);
        if (i == n_proc-1) end = N;
        recvcounts[i] = end - start;
        if (i == 0) displs[i] = 0;
        if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
    }
    // Gather TDs and cost func to ALL the processes
    for (int i = 0; i<n_proc; i++){
    MPI_Gatherv(sendarray, sendcount, MPI_DOUBLE,rbuf, recvcounts, displs,MPI_DOUBLE, i, comm);
    }
    vec.resize(N);
    for (int i = 0; i<N; i++) vec[i] = rbuf[i];
}



void mpi_gather_ivec(MPI_Comm comm, ivec & vec, int N)
{
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread

    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    int sendcount = vec.size();
    int sendarray[sendcount];
    for (int i = 0; i<sendcount; i++)
    {
        sendarray[i] = vec[i];
    }
    int rbuf[N];
    int recvcounts[n_proc];
    int displs[n_proc];
    for (int i = 0; i<n_proc; i++)
    {
        int start, end, span, step;
        if (n_proc==1) step = N;
        if (n_proc>1) step = (int) std::floor(N / (double) (n_proc));
        start = step * i;
        end   = step *(i+1);
        if (i == n_proc-1) end = N;
        recvcounts[i] = end - start;
        if (i == 0) displs[i] = 0;
        if (i > 0) displs[i] = displs[i-1] + recvcounts[i-1];
    }
    // Gather TDs and cost func to ALL the processes
    for (int i = 0; i<n_proc; i++){
    MPI_Gatherv(sendarray, sendcount, MPI_DOUBLE,rbuf, recvcounts, displs,MPI_DOUBLE, i, comm);
    }
    vec.resize(N);
    for (int i = 0; i<N; i++) vec[i] = rbuf[i];
}
