#include "bvp.h"



typedef  std::vector<double> dvec;
typedef  std::vector<int>    ivec;
typedef  std::vector<triangle> triangle_mesh;

//=========UNIFORM TENSION TEST PROBLEM==================

// Creates  configuration  of  sources  and  targets that
// correspond to the problem of simple tension on a  cube
// This test fully correspond to "h2 basic bem main" file
// in python project.





// This function generates (-1,1) cube with refinement given by N

void generate_vertices(
                        int N,

                        triangle_mesh & mesh

                      )
{
        //====================================================

        // Triangle vertices

        //======CUBE EDGE 1 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                mesh[4 * (i + j*N)].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N)].v1[1] = 1.0;
                mesh[4 * (i + j*N)].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N)].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N)].v2[1] = 1.0;
                mesh[4 * (i + j*N)].v2[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N)].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N)].v3[1] = 1.0;
                mesh[4 * (i + j*N)].v3[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
                //std::cout<<N<<" "<<i<<" "<<j<<" "<<vert_1[ 3 * (4 * (i + j*N) ) + 0]<<std::endl;
                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 1 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 1 ].v1[1] = 1.0;
                mesh[4 * (i + j*N) + 1 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 1 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 1 ].v2[1] = 1.0;
                mesh[4 * (i + j*N) + 1 ].v2[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 1 ].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 1 ].v3[1] = 1.0;
                mesh[4 * (i + j*N) + 1 ].v3[2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 2 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 2 ].v1[1] = 1.0;
                mesh[4 * (i + j*N) + 2 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 2 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 2 ].v2[1] = 1.0;
                mesh[4 * (i + j*N) + 2 ].v2[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 2 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 2 ].v3[1] = 1.0;
                mesh[4 * (i + j*N) + 2 ].v3[2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                mesh[4 * (i + j*N) + 3 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 3 ].v1[1] = 1.0;
                mesh[4 * (i + j*N) + 3 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 3 ].v2[0] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 3 ].v2[1] = 1.0;
                mesh[4 * (i + j*N) + 3 ].v2[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 3 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 3 ].v3[1] = 1.0;
                mesh[4 * (i + j*N) + 3 ].v3[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================



        //======CUBE EDGE 2 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){


                // ------------- element 1 ------------------
                mesh[4 * (i + j*N) + 4*N*N].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N].v1[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N].v3[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 4*N*N].v3[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N].v3[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 4*N*N].v2[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 4*N*N].v2[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N].v2[2] = - 1.0 + 2.0/N*(j);

                //-------------------------------------------

                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v1[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v3[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v3[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v3[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v2[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v2[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 1 ].v2[2] = - 1.0 + 2.0/N*(j+1);

                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v1[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v3[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v3[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v3[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v2[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v2[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 2 ].v2[2] = - 1.0 + 2.0/N*(j+1);

                //-------------------------------------------

                // ------------- element 4 ------------------

                mesh[4 * (i + j*N) + 4*N*N + 3 ].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v1[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v3[1] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v3[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v3[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v2[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v2[0] = 1.0;
                mesh[4 * (i + j*N) + 4*N*N + 3 ].v2[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 3 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                mesh[4 * (i + j*N) + 8*N*N].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N].v1[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 8*N*N].v3[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N].v3[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 8*N*N].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 8*N*N].v2[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N].v2[2] = - 1.0 + 2.0/N*(j);

                //-------------------------------------------

                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v1[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v3[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v3[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v2[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 1 ].v2[2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v1[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v3[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v3[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v2[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 2 ].v2[2] = - 1.0 + 2.0/N*(j+1);

                //-------------------------------------------

                // ------------- element 4 ------------------
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v1[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v3[0] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v3[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v3[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v2[1] = -1.0;
                mesh[4 * (i + j*N) + 8*N*N + 3 ].v2[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 4 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                mesh[4 * (i + j*N) + 12*N*N].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N].v1[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N].v2[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 12*N*N].v2[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N].v2[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 12*N*N].v3[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 12*N*N].v3[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N].v3[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 12*N*N+ 1].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v1[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v2[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 12*N*N+ 1].v2[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v2[2] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 12*N*N+ 1].v3[1] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v3[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N+ 1].v3[2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v1[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v2[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v2[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v2[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v3[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v3[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 2 ].v3[2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v1[1] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v1[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v1[2] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v2[1] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v2[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v2[2] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v3[1] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v3[0] = -1.0;
                mesh[4 * (i + j*N) + 12*N*N + 3 ].v3[2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 5 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                mesh[4 * (i + j*N) + 16*N*N].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N].v1[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 16*N*N].v3[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N].v3[1] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 16*N*N].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 16*N*N].v2[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N].v2[1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v1[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v3[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v3[1] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v2[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 1 ].v2[1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v1[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v3[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v3[1] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v2[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 2 ].v2[1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v1[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v3[0] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v3[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v3[1] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v2[2] = 1.0;
                mesh[4 * (i + j*N) + 16*N*N + 3 ].v2[1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 6 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                mesh[4 * (i + j*N) + 20*N*N].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N].v1[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N].v2[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 20*N*N].v2[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N].v2[1] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 20*N*N].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 20*N*N].v3[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N].v3[1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v1[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v2[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v2[1] = - 1.0 + 2.0/N*(j+1);
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v3[0] = 1.0 - 2.0/N*i;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v3[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 1 ].v3[1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v1[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v2[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v2[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v2[1] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v3[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 2 ].v3[1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v1[0] = 1.0 - 2.0/N*i - 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v1[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v1[1] = - 1.0 + 2.0/N*j + 1.0/N;
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v2[0] = 1.0 - 2.0/N*(i);
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v2[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v2[1] = - 1.0 + 2.0/N*(j);
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v3[0] = 1.0 - 2.0/N*(i+1);
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v3[2] = -1.0;
                mesh[4 * (i + j*N) + 20*N*N + 3 ].v3[1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }

        // Setting mesh ids
        for (int i = 0; i<mesh.size();i++)
        {
            mesh[i].id = i;
        }
}



void set_neighbors(triangle_mesh & mesh, double R)
{
    // Caution - N^2 complexity!

    for (int i = 0; i<mesh.size(); i++){
        for (int j = 0; j<mesh.size(); j++){
            if   (  (i!=j)&&(

                 (  (mesh[i].v1[0] - mesh[j].v1[0])*(mesh[i].v1[0] - mesh[j].v1[0]) +
                    (mesh[i].v1[1] - mesh[j].v1[1])*(mesh[i].v1[1] - mesh[j].v1[1]) +
                    (mesh[i].v1[2] - mesh[j].v1[2])*(mesh[i].v1[2] - mesh[j].v1[2]) < R*R )||
                 (  (mesh[i].v1[0] - mesh[j].v2[0])*(mesh[i].v1[0] - mesh[j].v2[0]) +
                    (mesh[i].v1[1] - mesh[j].v2[1])*(mesh[i].v1[1] - mesh[j].v2[1]) +
                    (mesh[i].v1[2] - mesh[j].v2[2])*(mesh[i].v1[2] - mesh[j].v2[2]) < R*R )||
                 (  (mesh[i].v1[0] - mesh[j].v3[0])*(mesh[i].v1[0] - mesh[j].v3[0]) +
                    (mesh[i].v1[1] - mesh[j].v3[1])*(mesh[i].v1[1] - mesh[j].v3[1]) +
                    (mesh[i].v1[2] - mesh[j].v3[2])*(mesh[i].v1[2] - mesh[j].v3[2]) < R*R )||
                 (  (mesh[i].v2[0] - mesh[j].v1[0])*(mesh[i].v2[0] - mesh[j].v1[0]) +
                    (mesh[i].v2[1] - mesh[j].v1[1])*(mesh[i].v2[1] - mesh[j].v1[1]) +
                    (mesh[i].v2[2] - mesh[j].v1[2])*(mesh[i].v2[2] - mesh[j].v1[2]) < R*R )||
                 (  (mesh[i].v2[0] - mesh[j].v2[0])*(mesh[i].v2[0] - mesh[j].v2[0]) +
                    (mesh[i].v2[1] - mesh[j].v2[1])*(mesh[i].v2[1] - mesh[j].v2[1]) +
                    (mesh[i].v2[2] - mesh[j].v2[2])*(mesh[i].v2[2] - mesh[j].v2[2]) < R*R )||
                 (  (mesh[i].v2[0] - mesh[j].v3[0])*(mesh[i].v2[0] - mesh[j].v3[0]) +
                    (mesh[i].v2[1] - mesh[j].v3[1])*(mesh[i].v2[1] - mesh[j].v3[1]) +
                    (mesh[i].v2[2] - mesh[j].v3[2])*(mesh[i].v2[2] - mesh[j].v3[2]) < R*R )||
                 (  (mesh[i].v3[0] - mesh[j].v1[0])*(mesh[i].v3[0] - mesh[j].v1[0]) +
                    (mesh[i].v3[1] - mesh[j].v1[1])*(mesh[i].v3[1] - mesh[j].v1[1]) +
                    (mesh[i].v3[2] - mesh[j].v1[2])*(mesh[i].v3[2] - mesh[j].v1[2]) < R*R )||
                 (  (mesh[i].v3[0] - mesh[j].v2[0])*(mesh[i].v3[0] - mesh[j].v2[0]) +
                    (mesh[i].v3[1] - mesh[j].v2[1])*(mesh[i].v3[1] - mesh[j].v2[1]) +
                    (mesh[i].v3[2] - mesh[j].v2[2])*(mesh[i].v3[2] - mesh[j].v2[2]) < R*R )||
                 (  (mesh[i].v3[0] - mesh[j].v3[0])*(mesh[i].v3[0] - mesh[j].v3[0]) +
                    (mesh[i].v3[1] - mesh[j].v3[1])*(mesh[i].v3[1] - mesh[j].v3[1]) +
                    (mesh[i].v3[2] - mesh[j].v3[2])*(mesh[i].v3[2] - mesh[j].v3[2]) < R*R )  )

                    )


            mesh[i].neighbors.push_back(mesh[j].id);

        }
    }

}



//=======================================
//   CUBE MIXED TEST BVP
//=======================================

void set_cub_bvp(

        int N,

        // General BVP parameters
        triangle_mesh & mesh,

        bc_vec & bcs)

{ // Begining of the function

    // Generate triangle vertices
    generate_vertices(N, mesh);

    // Transform vertices to (0;1) bounding box
    double margin = 1e-10; //needed to ensure we are within bbox
    for (int i = 0; i<mesh.size();i++)
    {
        for (int j = 0; j<3;j++)
        {
            mesh[i].v1[j] = 0.5 + mesh[i].v1[j] * ( ( 1. - 2. * margin ) / 2. );
            mesh[i].v2[j] = 0.5 + mesh[i].v2[j] * ( ( 1. - 2. * margin ) / 2. );
            mesh[i].v3[j] = 0.5 + mesh[i].v3[j] * ( ( 1. - 2. * margin ) / 2. );
        }
    }

    // After geometric transformations we can define geometric neighbours
    set_neighbors(mesh, 0.00001);

    // type of boundary conditions
    // 1 - tractions are given, 2 - displacements are given
    for (int i = 0;      i<20*N*N; i++) bcs[i].bc_type = 1;
    for (int i = 20*N*N; i<24*N*N; i++) bcs[i].bc_type = 2;

    dvec tractions(N * N * 20 * 3);
    dvec displacements(N * N * 4 * 3);

    // tractions
    for (int i = 0; i<20*N*N; i++)
    {for (int k = 0; k<3; k++) tractions[3*i+k] = 0.0;}
    for (int i = 16*N*N; i<20*N*N; i++)
    { tractions[3*i+2] = 1.0;}

    // displacements
    for (int i = 0; i<4*N*N; i++)
    {for (int k = 0; k<3; k++) displacements[3*i+k] = 0.0;}

    // Combined RHS vector
    for (int i = 0; i<20*N*N; i++) {
        for (int k = 0; k<3; k++) {
            bcs[i].bc_value[k] = tractions[3*i+k];
        }
    }
    for (int i = 0; i<4*N*N; i++) {
        for (int k = 0; k<3; k++) {
            bcs[20*N*N + i].bc_value[k] = displacements[3*i+k];
        }
    }

} // End of function



/*

//=======================================
//   PURE DIRICHLET SPHERE TEST PROBLEM
//=======================================

void set_sph_bvp(

        int N,

        // General BVP parameters
        dvec & vert_1,
        dvec & vert_2,
        dvec & vert_3,

        ivec & bc_type,

        dvec & tr_di)

{ // Begining of the function

    // Generate triangle vertices
    generate_vertices(N, vert_1, vert_2, vert_3);

    // Transform vertices to (0;1) bounding box
    double margin = 1e-10; //needed to ensure we are within bbox
    for (int i = 0; i<vert_1.size();i++)
    {
        vert_1[i] = 0.5 + vert_1[i] * ( ( 1. - 2. * margin ) / 2. );
        vert_2[i] = 0.5 + vert_2[i] * ( ( 1. - 2. * margin ) / 2. );
        vert_3[i] = 0.5 + vert_3[i] * ( ( 1. - 2. * margin ) / 2. );
    }

    // Project vertices on a sphere with the radius R-margin
    // Project box on the sphere of the radius Rad
    double L;
    double Rad = 0.5 - margin;
    double c_0 = 0.5;
    double c_1 = 0.5;
    double c_2 = 0.5;
    for (int i = 0; i<vert_1.size()/3;i++)
    {

        L = std::sqrt(
                    (vert_1[3*i + 0] - c_0) * (vert_1[3*i + 0] - c_0) +
                    (vert_1[3*i + 1] - c_1) * (vert_1[3*i + 1] - c_1) +
                    (vert_1[3*i + 2] - c_2) * (vert_1[3*i + 2] - c_2)
                );
        vert_1[3*i + 0] = c_0 + (Rad/L) * ( vert_1[3*i + 0] - c_0 );
        vert_1[3*i + 1] = c_1 + (Rad/L) * ( vert_1[3*i + 1] - c_1 );
        vert_1[3*i + 2] = c_2 + (Rad/L) * ( vert_1[3*i + 2] - c_2 );

        L = std::sqrt(
                    (vert_2[3*i + 0] - c_0) * (vert_2[3*i + 0] - c_0) +
                    (vert_2[3*i + 1] - c_1) * (vert_2[3*i + 1] - c_1) +
                    (vert_2[3*i + 2] - c_2) * (vert_2[3*i + 2] - c_2)
                );
        vert_2[3*i + 0] = c_0 + (Rad/L) * ( vert_2[3*i + 0] - c_0 );
        vert_2[3*i + 1] = c_1 + (Rad/L) * ( vert_2[3*i + 1] - c_1 );
        vert_2[3*i + 2] = c_2 + (Rad/L) * ( vert_2[3*i + 2] - c_2 );

        L = std::sqrt(
                    (vert_3[3*i + 0] - c_0) * (vert_3[3*i + 0] - c_0) +
                    (vert_3[3*i + 1] - c_1) * (vert_3[3*i + 1] - c_1) +
                    (vert_3[3*i + 2] - c_2) * (vert_3[3*i + 2] - c_2)
                );
        vert_3[3*i + 0] = 0.5 + (Rad/L) * ( vert_3[3*i + 0] - 0.5 );
        vert_3[3*i + 1] = 0.5 + (Rad/L) * ( vert_3[3*i + 1] - 0.5 );
        vert_3[3*i + 2] = 0.5 + (Rad/L) * ( vert_3[3*i + 2] - 0.5 );


    }

    // type of boundary conditions
    // 1 - tractions are given, 2 - displacements are given
    for (int i = 0;      i<24*N*N; i++) bc_type[i] = 2;

    // displacements
    for (int i = 0; i<24*N*N; i++)
    {
        for (int k = 0; k<3; k++) tr_di[3*i+k] = 0;
        tr_di[3*i+2] =
        ((1./3.) * (vert_1[3*i+2] + vert_2[3*i+2] + vert_3[3*i+2]) - 0.5) / 2.;
    }

} // End of function



*/
