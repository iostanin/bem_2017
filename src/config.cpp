#include "config.h"
#include "colors.h"

using namespace std;

// Global parameters (see main.cpp)
extern int mode;
extern int kernel;
extern int omp;
extern int mult_order;
extern int summation;
extern int N;


void load_main_config() // Load main configuration file
{

      cout << purple << "Loading main configuration file..." << reset <<endl;
      ifstream fin;
      fin.open("./cfg/config.txt"); // open a file
      if (!fin.good())
      { cout << yellow << "Main configuration file not found, proceed with default parameters" << reset << endl; return; }

      // read each line of the file
      while (!fin.eof())
      {
        // read an entire line into memory
        char buf[100]; fin.getline(buf, 100);

        // parse the line into blank-delimited tokens
        int n = 0; // a for-loop index

        // array to store memory addresses of the tokens in buf
        char* token[3] = {}; // initialize to 0

        // parse the line
        token[0] = strtok(buf, " "); // first token
        if (token[0]) // zero if line is blank
        {
          for (n = 1; n < 3; n++)
          {
            token[n] = strtok(0, " "); // subsequent tokens
            if (!token[n]) break; // no more tokens
          }
        }

        if (token[2]) { // if there are 3 words in this line
        string word(token[0]);
        string value(token[2]);

        if ( word=="mode" )
        { mode = std::atoi(value.c_str());
          cout << cyan << "mode = " << mode << reset<<endl;}

        if ( word=="kernel" )
        { kernel = std::atoi(value.c_str());
          cout << cyan << "kernel = " << kernel << reset<<endl;}

        if ( word=="omp" )
        { omp = std::atoi(value.c_str());
          cout << cyan << "omp = " << omp << reset<<endl;}

        if ( word=="mult_order" )
        { mult_order = std::atoi(value.c_str());
          cout << cyan << "mult_order = " << mult_order << reset<<endl;}

        if ( word=="summation" )
        { summation = std::atoi(value.c_str());
          cout << cyan << "summation = " << summation << reset<<endl;}

        if ( word=="N" )
        { N = std::atoi(value.c_str());
          cout << cyan << "N = " << N << reset<<endl;}



        }

      }
    cout << purple << "Done" << reset << endl <<endl;
    return;
}


