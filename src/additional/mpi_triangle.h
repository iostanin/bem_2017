#ifndef MPI_TRIANGLE_H
#define MPI_TRIANGLE_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include <iostream>
#include <vector>
#include <cmath>

#include <omp.h>
#include <mpi.h>

typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

typedef struct triangle_s {

        int id;

        double x1;
        double y1;
        double z1;


} triangle;



MPI_Datatype create_mpi_triangle(){
    /* create a type for struct car */
    const int nitems=4;
    int          blocklengths[nitems] = {1,1,1,1};
    MPI_Datatype mpi_triangle_type;
    MPI_Datatype types[nitems] = {MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE};
    MPI_Aint     offsets[nitems];



    offsets[0] = offsetof(triangle, id);
    offsets[1] = offsetof(triangle, x1);
    offsets[2] = offsetof(triangle, y1);
    offsets[3] = offsetof(triangle, z1);


    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_triangle_type);
    MPI_Type_commit(&mpi_triangle_type);

    return mpi_triangle_type;
}
#endif // MPI_TRIANGLE_H
