/// ========Kernel Independent Boundary Element Method===========
/// ========Shape  and  Topology Optimization Software===========
/// =====Program by I. Ostanin, I. Tsybulin, D.Zorin, 2017=======



///---------------Main entry point-------------------------------




///---------------Global parameters------------------------------

//  These default values can be overwritten
//  with ones in configuration file!

int mode = 1; // working mode

///    The following modes are available:
///        1) mode = 1: Run a set of validation test problems
///           and measure the performance
///
///        2) mode = 2: Solve a boundary value problem
///
///        3) mode = 3: Optimize shape
///
///        4) mode = 4: Optimize topology
///
///        5) mode = 5: Mixed optimization

int kernel = 1; // type of integral kernel

///    The following kernels and BVPs are available:
///
///        1) kernel = 1: Elasticity  (works)
///
///        2) kernel = 2: Electrostatic
///
///        3) kernel = 3: Stokes flow

int omp = 8; // number of OMP threads

/// The utilized number of threads should not exceed
/// the number of available cores

int mult_order = 6; // PVFMM multipole order

/// Multipole odrer should be even integrer number:
/// 2, 4, 6, 8, 10 ...

int summation = 2; // Type of summation algorithm utilized by matvecs

///  summatiion = 1 - direct summation (works ok only in serial mode)
///  summation = 2 - kifmm summation

int N = 1; // Spatial definition

/// The (0,1)^3 working domain is divided on N X N X N cells


///==============================================================


///--------Includes---------

#include <mpi.h>
#include <omp.h>


#include "colors.h"
#include "config.h"

#include "elast_tests.h"
#include "elast_solve.h"
#include "elast_shape.h"
#include "elast_topol.h"
#include "elast_mixed.h"

#include "laplace_tests.h"
#include "laplace_solve.h"
#include "laplace_shape.h"
#include "laplace_topol.h"
#include "laplace_mixed.h"

///=========================

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include <stddef.h>





int main(int argc, char **argv) {
    cout << endl << endl << purple << "KERNEL INDEPENDENT BEM SOLVER/OPTIMIZER" << reset << endl << endl;

    // Read the configuration file
    load_main_config();

    // Init MPI
    MPI_Init(&argc, &argv);
    MPI_Comm comm=MPI_COMM_WORLD;

    // Init OMP
    omp_set_num_threads( omp );

    // Run the appropriate solver
    if ((mode == 1) && (kernel == 1)) run_cub_test_elast(comm);
    if ((mode == 2) && (kernel == 1)) run_solve_elast();
    if ((mode == 3) && (kernel == 1)) run_shape_elast();
    if ((mode == 4) && (kernel == 1)) run_topol_elast();
    if ((mode == 5) && (kernel == 1)) run_mixed_elast();


    if ((mode == 1) && (kernel == 2)) run_tests_laplace();
    if ((mode == 2) && (kernel == 2)) run_solve_laplace();
    if ((mode == 3) && (kernel == 2)) run_shape_laplace();
    if ((mode == 4) && (kernel == 2)) run_topol_laplace();
    if ((mode == 5) && (kernel == 2)) run_mixed_laplace();

}
