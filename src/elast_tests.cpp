#include "elast_tests.h"



typedef std::vector<double> dvec;
typedef std::vector<int> ivec;

using std::endl;
using std::cout;

extern int omp;
extern int mult_order;
extern int N;

bool full_output = true;

//-----Default elastic moduli------
double SHEAR = 1;
double POISS = 0;
double YOUNG = 2.*SHEAR*(1.+POISS);
//=================================

// Number of GMRES iteration
int N_iter = 0;

/*
void run_sph_test_elast(MPI_Comm comm)
{
    // MPI process indices
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    if (!i_proc) cout << endl << cyan << " ELASTICITY KERNEL TESTS " << reset << endl << endl;
    if (!i_proc) cout << endl << purple << " 1) Solution of the standard cube test problem " << reset << endl << endl;




    // SPHERE
    // --------1) Set up serial mesh------------------------------------
    if (!i_proc) cout << green << "Generate mesh (sphere test problem)... " << reset;
    int N_trg = N * N * 24;

    dvec vert_1( N_trg * 3);
    dvec vert_2( N_trg * 3);
    dvec vert_3( N_trg * 3);
    dvec tr_di(N_trg * 3);
    ivec bc_type( N_trg );

    set_sph_bvp(N, vert_1, vert_2, vert_3, bc_type, tr_di);

    if (!i_proc) cout << green << "done" << reset << endl;

    if (full_output) {

            if (!i_proc) {
            print_dvec3( "vert_1", vert_1);
            print_dvec3( "vert_2", vert_2);
            print_dvec3( "vert_3", vert_3);
            print_ivec( "bc_type", bc_type);
            print_dvec3( "tr_di", tr_di);
            }

    }
    //===============================================================




    //------2) Distribute mesh---------------------------------------
    mpi_scatter_dvec3(comm, vert_1);
    mpi_scatter_dvec3(comm, vert_2);
    mpi_scatter_dvec3(comm, vert_3);
    mpi_scatter_dvec3(comm, tr_di);
    mpi_scatter_ivec(comm, bc_type);

    if (full_output) {
    //        if (!i_proc) cout << cyan << "SCATTERED" << reset <<endl;
    //
    //        for (int i = 0; i<n_proc; i++) {
    //            if (i_proc==i) print_ivec( "bc_type", bc_type);
    //            usleep(1000000);
    //        }
    }
    //===============================================================



    //----3) Solution on the surface----------------------------------------

    // Cash vector with knowns
    dvec buff(tr_di.size());
    for (int i = 0; i<tr_di.size(); i++)  buff[i] = tr_di[i];

    SolverFmmGmres * slvr = new SolverFmmGmres();
    slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di, mult_order, comm);
    slvr->solve(mult_order, comm, 1000, 1e-7);
    delete slvr;

    // Sort the obtained solution onto tractions and displacements

    dvec tractions(buff.size()), displacements(buff.size());

    for (int i = 0; i<bc_type.size();i++) {

        if (bc_type[i] == 1) {
            for (int k = 0; k<3; k++) {
                tractions[3*i+k] =  buff[3*i+k];
                displacements[3*i+k] = tr_di[3*i+k];
            }
        }

        if (bc_type[i] == 2) {
            for (int k = 0; k<3; k++) {
                tractions[3*i+k] = tr_di[3*i+k];
                displacements[3*i+k] = buff[3*i+k];
            }
        }
    }


    ///============================================================

    if (!i_proc) { print_dvec3( "tractions", tractions);}
    if (!i_proc) { print_dvec3( "displacements", displacements);}

    // Gather tractions
    mpi_gather_dvec(comm, tractions, N_trg*3);

    if (!i_proc) { print_dvec3( "tractions after gather", tractions);}



    dvec tractions_an(tractions.size());
    for (int i = 0; i<tractions_an.size(); i++)
    tractions_an[i] = 0;
    // Stresses reconstruction
    dvec z_stress(N_trg);
    dvec v_1(3), v_2(3), v_3(3), n(3);
    for (int i = 0; i<N_trg; i++)
    {
        for (int k = 0; k<3; k++)
        {
            v_1[k] = vert_1[3*i+k];
            v_2[k] = vert_2[3*i+k];
            v_3[k] = vert_3[3*i+k];
        }

        n = norm(v_1, v_2, v_3);
        tractions_an[3*i+2] = n[2];

        if ( fabs(n[2]) > 10e-3)
        {
            z_stress[i] = tractions [3*i + 2] / n[2];
        }
        else
        {
            z_stress[i] = 0;
        }
    }




    if (!i_proc) { print_dvec( "z_stress", z_stress);}


    // Determine maximum deviation of numerical
    // solution from an analytical one ( L_inf )

    double L_inf = 0, L;
    for (int i = 0; i<N_trg; i++)
    {
        if (fabs(z_stress[i])>10e-8) // Not one of those vals we could not recover
        {
            L = fabs(z_stress[i] - 1.);
            if ( L > L_inf ) L_inf  = L;
        }
    }

    double L_2 = 0, L_2s = 0;

    for (int i = 0; i<N_trg; i++)
    {
        L_2 += pow(z_stress[i] - 1., 2.);
        L_2s += pow(z_stress[i], 2.);

    }
    L_2 = sqrt(L_2);
    L_2s = sqrt(L_2s);
    L_2 = L_2 / L_2s;


    cout << red <<"relative L_inf of the error in stress is "<< L_inf << reset <<endl;
    cout << green <<"relative L_2 of the error in stress is "<< L_2 << reset <<endl;


    L = 0; L_inf = 0; L_2 = 0; L_2s = 0;
    for (int i = 0; i<tractions_an.size(); i++)
    {
            L = fabs(tractions_an[i] - tractions[i]);
            if ( L > L_inf ) L_inf  = L;
    }


    for (int i = 0; i<tractions_an.size(); i++)
    {
        L_2 += pow(tractions_an[i] - tractions[i], 2.);
        L_2s += pow(tractions_an[i], 2.);

    }
    L_2 = sqrt(L_2);
    L_2s = sqrt(L_2s);
    L_2 = L_2 / L_2s;


    cout << red <<"relative L_inf of the error in tractions is "<< L_inf << reset <<endl;
    cout << green <<"relative L_2 of the error in tractions is "<< L_2 << reset <<endl;


    write_stl("./stl/elast_tests/sphere.stl", vert_1, vert_2, vert_3);

    write_vert("./io/elast_tests/vertices.txt", vert_1, vert_2, vert_3);
    write_vect("./io/elast_tests/tractions.txt", tractions);
    write_vect("./io/elast_tests/displacements.txt", displacements);

}
*/

void run_cub_test_elast(MPI_Comm comm)
{


    // MPI process indices
    int n_proc; // Number of MPI threads
    int i_proc; // index of this MPI thread
    MPI_Comm_size(comm, &n_proc);
    MPI_Comm_rank(comm, &i_proc);

    if (!i_proc) cout << endl << cyan << " ELASTICITY KERNEL TESTS " << reset << endl << endl;
    if (!i_proc) cout << endl << purple << " 1) Solution of the standard cube test problem " << reset << endl << endl;

    // CUBE
    // --------1) Set up serial mesh------------------------------------
    if (!i_proc) cout << green << "Generate mesh (cube test problem)... " << reset;
    int N_trg = N * N * 24;

    triangle_mesh mesh(N_trg);
    bc_vec bcs(N_trg);
    set_cub_bvp(N, mesh, bcs);

    print_triangle_mesh("BVP 1", mesh);
    print_bc_vec("BVP 1", bcs);
    write_STL("./stl/elast_tests/new_format", mesh);
    return;
    /*
    dvec vert_1( N_trg * 3);
    dvec vert_2( N_trg * 3);
    dvec vert_3( N_trg * 3);
    dvec tr_di(N_trg * 3);
    ivec bc_type( N_trg );

    set_cub_bvp(N, vert_1, vert_2, vert_3, bc_type, tr_di);

    if (!i_proc) cout << green << "done" << reset << endl;

    if (full_output) {

            if (!i_proc) {
            print_dvec3( "vert_1", vert_1);
            print_dvec3( "vert_2", vert_2);
            print_dvec3( "vert_3", vert_3);
            print_ivec( "bc_type", bc_type);
            print_dvec3( "tr_di", tr_di);
            }

    }
    //===============================================================




    //------2) Distribute mesh---------------------------------------
    mpi_scatter_dvec3(comm, vert_1);
    mpi_scatter_dvec3(comm, vert_2);
    mpi_scatter_dvec3(comm, vert_3);
    mpi_scatter_dvec3(comm, tr_di);
    mpi_scatter_ivec(comm, bc_type);

    if (full_output) {
    //        if (!i_proc) cout << cyan << "SCATTERED" << reset <<endl;
    //
    //        for (int i = 0; i<n_proc; i++) {
    //            if (i_proc==i) print_ivec( "bc_type", bc_type);
    //            usleep(1000000);
    //        }
    }
    //===============================================================



    //----3) Solution on the surface----------------------------------------

    // Cash vector with knowns
    dvec buff(tr_di.size());
    for (int i = 0; i<tr_di.size(); i++)  buff[i] = tr_di[i];

    SolverFmmGmres * slvr = new SolverFmmGmres();
    slvr->init(N_trg, vert_1, vert_2, vert_3, bc_type, tr_di, mult_order, comm);
    slvr->solve(mult_order, comm, 1000, 1e-7);
    delete slvr;

    // Sort the obtained solution onto tractions and displacements

    dvec tractions(buff.size()), displacements(buff.size());

    for (int i = 0; i<bc_type.size();i++) {

        if (bc_type[i] == 1) {
            for (int k = 0; k<3; k++) {
                tractions[3*i+k] =  buff[3*i+k];
                displacements[3*i+k] = tr_di[3*i+k];
            }
        }

        if (bc_type[i] == 2) {
            for (int k = 0; k<3; k++) {
                tractions[3*i+k] = tr_di[3*i+k];
                displacements[3*i+k] = buff[3*i+k];
            }
        }
    }


    ///============================================================

    if (!i_proc) { print_dvec3( "tractions", tractions);}
    if (!i_proc) { print_dvec3( "displacements", displacements);}

    // Gather tractions
    mpi_gather_dvec(comm, tractions, N_trg*3);

    if (!i_proc) { print_dvec3( "tractions after gather", tractions);}

    return;

    // Analytical solution - only serial mode!

    dvec tractions_an(tractions.size());
    dvec displacements_an(displacements.size());

    for (int i = 0; i<tractions.size(); i++) tractions_an[i] = 0;
    for (int i = 16*N*N; i<20*N*N; i++) tractions_an[3*i+2] = 1.;
    for (int i = 20*N*N; i<24*N*N; i++) tractions_an[3*i+2] = -1.;

    for (int i = 0; i<displacements.size(); i++) displacements_an[i] = 0;
    for (int i = 0; i<displacements.size()/3; i++) displacements_an[3*i+2] = 0.5 * (1./3.) * (vert_1[3.*i+2.] + vert_2[3.*i+2.] + vert_3[3.*i+2.]);

    print_dvec3( "tractions_an", tractions_an);
    print_dvec3( "displacements_an", displacements_an);


    // Determine maximum deviation of numerical
    // solution from an analytical one ( L_inf ) for displacements

    double L_inf = 0, L;

    for (int i = 0; i<displacements_an.size(); i++)
    {
            L = fabs(displacements_an[i] - displacements[i]);
            if ( L > L_inf ) L_inf  = L;
    }

    double L_2 = 0, L_2s = 0;

    for (int i = 0; i<displacements_an.size(); i++)
    {
        L_2 += pow(displacements_an[i] - displacements[i], 2.);
        L_2s += pow(displacements_an[i], 2.);

    }
    L_2 = sqrt(L_2);
    L_2s = sqrt(L_2s);
    L_2 = L_2 / L_2s;


    cout << red <<"relative L_inf of the error in displacements is "<< L_inf << reset <<endl;
    cout << green <<"relative L_2 of the error in displacements is "<< L_2 << reset <<endl;

    L = 0; L_inf = 0; L_2 = 0; L_2s = 0;
    for (int i = 0; i<tractions_an.size(); i++)
    {
            L = fabs(tractions_an[i] - tractions[i]);
            if ( L > L_inf ) L_inf  = L;
    }


    for (int i = 0; i<tractions_an.size(); i++)
    {
        L_2 += pow(tractions_an[i] - tractions[i], 2.);
        L_2s += pow(tractions_an[i], 2.);

    }
    L_2 = sqrt(L_2);
    L_2s = sqrt(L_2s);
    L_2 = L_2 / L_2s;


    cout << red <<"relative L_inf of the error in tractions is "<< L_inf << reset <<endl;
    cout << green <<"relative L_2 of the error in tractions is "<< L_2 << reset <<endl;



    write_stl("./stl/elast_tests/cube.stl", vert_1, vert_2, vert_3);

*/

}
