def classic_plot(pro, c, M, N, K, tractions, displacements):
    import matplotlib.pyplot as plt
    import numpy as np
    margin = 0.01
    ymargin = 0.01
    
    C = 0.3
    
    
    # Attaching 3D axis to the figure
    fig1 = plt.figure(figsize = [18,18], dpi = 300)
    #axe = p3.Axes3D(fig)
    axe1 = fig1.add_subplot(111, projection='3d')
    
    fig2 = plt.figure(figsize = [18,18], dpi = 600)
    #axe = p3.Axes3D(fig)
    axe2 = fig2.add_subplot(111, projection='3d')
    
    fig3 = plt.figure(figsize = [18,18], dpi = 600)
    #axe = p3.Axes3D(fig)
    axe3 = fig3.add_subplot(111, projection='3d')
    
    axe3.set_xlim3d([-margin, c * M + margin])
    axe3.set_ylim3d([-ymargin, c * N + ymargin])
    axe3.set_zlim3d([-margin, c * K + margin])
    
    
    # Octree cell centers
    
    for lev in range(pro.foc.maxlevel+1):      
        for i in range (pro.foc.number_l[lev], pro.foc.number_l[lev + 1]):
            if ((pro.foc.list_of_cells[i].isLeaf)&(pro.foc.list_of_cells[i].status)):
                xx = pro.foc.list_of_cells[i].position[0]
                yy = pro.foc.list_of_cells[i].position[1]
                zz = pro.foc.list_of_cells[i].position[2]
                axe1.scatter(xx, yy, zz, color = "green")
            if ((pro.foc.list_of_cells[i].isLeaf)&(not pro.foc.list_of_cells[i].status)):
                xx = pro.foc.list_of_cells[i].position[0]
                yy = pro.foc.list_of_cells[i].position[1]
                zz = pro.foc.list_of_cells[i].position[2]
                axe1.scatter(xx, yy, zz, color = "red")
            
    
    # normals at colloc points
    xs = np.zeros(pro.N_ele)
    ys = np.zeros(pro.N_ele)
    zs = np.zeros(pro.N_ele)
    for i in range (0, pro.N_ele):
        # Edge 1
        point1 = pro.colloc[i]
        point2 = pro.colloc[i] + C*0.2 * pro.norm[i]
        xs[i] = point2[0]
        ys[i] = point2[1]
        zs[i] = point2[2]
    
        x = np.array([point1[0], point2[0]])
        y = np.array([point1[1], point2[1]])
        z = np.array([point1[2], point2[2]])
        #axe1.plot_wireframe(x,y,z, color = 'green')        
    #axe1.scatter(xs, ys, zs, color = "green")
    
    axe1.set_xlim3d([-margin, c * M + margin])
    axe1.set_ylim3d([-ymargin, c * N + ymargin])
    axe1.set_zlim3d([-margin, c * K + margin])
    axe1.text2D(0.02, 0.05, "Normals",fontsize=30, color = "green")
        
    # Original configuration - wireframe
    for i in range (0, pro.N_ele):
        
        
        # Edge 1
        point1 = pro.vert_1[i]
        point2 = pro.vert_2[i]
        point3 = pro.vert_3[i]
        #"""
        x = np.array([point1[0], point2[0]])
        y = np.array([point1[1], point2[1]])
        z = np.array([point1[2], point2[2]])
        axe1.plot_wireframe(x,y,z, color = 'grey')
        axe2.plot_wireframe(x,y,z, color = 'grey')
        axe3.plot_wireframe(x,y,z, color = 'grey')
        #"""
        # Edge 2
        x = np.array([point2[0], point3[0]])
        y = np.array([point2[1], point3[1]])
        z = np.array([point2[2], point3[2]])
        axe1.plot_wireframe(x,y,z, color = 'grey')
        axe2.plot_wireframe(x,y,z, color = 'grey')
        axe3.plot_wireframe(x,y,z, color = 'grey')
        #"""
        # Edge 3
        x = np.array([point1[0], point3[0]])
        y = np.array([point1[1], point3[1]])
        z = np.array([point1[2], point3[2]])
        axe1.plot_wireframe(x,y,z, color = 'grey')
        axe2.plot_wireframe(x,y,z, color = 'grey')
        axe3.plot_wireframe(x,y,z, color = 'grey')
        #"""
        
    # displacements at colloc points
    xs = np.zeros(pro.N_ele)
    ys = np.zeros(pro.N_ele)
    zs = np.zeros(pro.N_ele)
    
    for i in range (0, pro.N_ele):
        # Edge 1
        point1 = pro.colloc[i]
        point2 = pro.colloc[i] + C * displacements[i]
        xs[i] = point2[0]
        ys[i] = point2[1]
        zs[i] = point2[2]
    
        x = np.array([point1[0], point2[0]])
        y = np.array([point1[1], point2[1]])
        z = np.array([point1[2], point2[2]])
        axe2.plot_wireframe(x,y,z, color = 'red')        
    
    axe2.scatter(xs, ys, zs, color = "red")
    
    axe2.set_xlim3d([-margin, c * M + margin])
    axe2.set_ylim3d([-ymargin, c * N + ymargin])
    axe2.set_zlim3d([-margin, c * K + margin])
    axe2.text2D(0.02, 0.05, "Displacements",fontsize=30, color = "red")
    
    
    
    
    # tractions at colloc points
    xs = np.zeros(pro.N_ele)
    ys = np.zeros(pro.N_ele)
    zs = np.zeros(pro.N_ele)
    for i in range (0, pro.N_ele):
        # Edge 1
        point1 = pro.colloc[i]
        point2 = pro.colloc[i] + C * tractions[i]
        xs[i] = point2[0]
        ys[i] = point2[1]
        zs[i] = point2[2]
    
        x = np.array([point1[0], point2[0]])
        y = np.array([point1[1], point2[1]])
        z = np.array([point1[2], point2[2]])
        axe3.plot_wireframe(x,y,z, color = 'orange')        
    axe3.scatter(xs, ys, zs, color = "orange")
    
    axe3.set_xlim3d([-margin, c * M + margin])
    axe3.set_ylim3d([-ymargin, c * N + ymargin])
    axe3.set_zlim3d([-margin, c * K + margin])
    axe3.text2D(0.02, 0.05, "Forces",fontsize=30, color = "orange")
    
        
    
    plt.show()